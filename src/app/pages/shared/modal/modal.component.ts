import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
  @Input() title = 'Title';
  @Input() body = 'Are you sure?';
  @Input() actionType = 'btn-primary';
  @Input() actionName = 'Ok';

  constructor(public activeModal: NgbActiveModal) {}
}
