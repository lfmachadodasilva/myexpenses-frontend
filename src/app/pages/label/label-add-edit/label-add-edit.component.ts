import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { faEdit, faPlus, faUndo } from '@fortawesome/free-solid-svg-icons';

import { LabelModel } from 'src/app/models/label.model';
import { HttpLabelService } from 'src/app/services/http/http-label.service';
import { ErrorConst } from '../../shared/error.const';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-label-add-edit',
  templateUrl: './label-add-edit.component.html',
  styleUrls: ['./label-add-edit.component.scss']
})
export class LabelAddEditComponent implements OnInit {
  labelForm: FormGroup;

  error: string = null;
  isLoading = false;
  isLoadingEditMode = false;
  isLoadingEditModeSubmit = false;
  editMode = false;

  labelId: number;
  label: LabelModel;

  imgEdit = faEdit;
  imgAdd = faPlus;
  imgCancel = faUndo;

  defaultGroupId = 1;

  constructor(
    private labelService: HttpLabelService,
    private route: ActivatedRoute,
    private router: Router,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.defaultInitForm();

    this.route.params.subscribe((params: Params) => {
      this.editMode = params.id !== null && params.id !== undefined;

      if (this.editMode) {
        this.labelId = params.id;

        this.isLoadingEditMode = true;
        this.labelService
          .getLabel(this.labelId)
          .then(label => {
            this.label = label;
            this.labelForm = new FormGroup({
              name: new FormControl(this.label.name, Validators.required)
            });
          })
          .catch(reason => this.catchError(reason, null))
          .finally(() => {
            this.isLoadingEditMode = false;
          });
      } else {
        this.defaultInitForm();
      }
    });
  }

  onSubmit() {
    if (this.editMode) {
      const l = new LabelModel(this.labelId, this.labelForm.value.name);
      this.isLoadingEditModeSubmit = true;
      this.labelService
        .updateLabel(l)
        .then(() => {
          this.router.navigate(['/label'], { relativeTo: this.route });
        })
        .catch(reason => this.catchError(reason, l.name))
        .finally(() => {
          this.isLoadingEditModeSubmit = false;
          this.editMode = false;
        });
    } else {
      const l = new LabelModel(-1, this.labelForm.value.name);
      l.groupId = this.defaultGroupId;
      this.isLoading = true;
      this.labelService
        .addLabel(l)
        .then(() => {
          this.router.navigate(['/label'], { relativeTo: this.route });
        })
        .catch(reason => this.catchError(reason, l.name))
        .finally(() => {
          this.isLoading = false;
        });
    }
  }

  private catchError(reason: any, name: string) {
    if (reason.error === ErrorConst.duplicate) {
      this.error = `Duplicate label with name: ${name}. Try another one.`;
    } else {
      this.error = this.translateService.instant('ERROR.GENERAL');
    }
    // if (reason.error === ErrorConst.duplicate) {
    //   this.error = `Duplicate label with name: ${name}. Try another one.`;
    // } else {
    //   this.error = 'Something went wrong, try again later.';
    // }
  }

  private defaultInitForm() {
    this.labelForm = new FormGroup({
      name: new FormControl(null, Validators.required)
    });
  }
}
