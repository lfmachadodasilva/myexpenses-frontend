import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import * as Highcharts from 'highcharts';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { faEdit, faTrash, faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';

import { HttpLabelService } from 'src/app/services/http/http-label.service';
import { LabelWithValuesModel } from 'src/app/models/label.model';
import { ModalComponent } from '../../shared/modal/modal.component';
import { SearchService } from 'src/app/services/search.service';
import { SearchModel } from 'src/app/models/search.model';

@Component({
  selector: 'app-label-list',
  templateUrl: './label-list.component.html',
  styleUrls: ['./label-list.component.scss']
})
export class LabelListComponent implements OnInit, OnDestroy {
  labels: LabelWithValuesModel[];
  isLoading = false;
  isLoadingDelete: number = null;
  error: string = null;

  defaultGroupId = -1;
  defaultMonth = -1;
  defaultYear = -1;

  searchSubscription: Subscription;

  imgEdit = faEdit;
  imgDelete = faTrash;
  imgAdd = faPlus;
  imgSearch = faSearch;

  Highcharts = Highcharts;
  chartOptions = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'LABEL.GRAPH.TITLE'
    },
    tooltip: {
      pointFormat: '{series.name}:<b> {point.y:.2f} ({point.percentage:.2f}%)</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}:</b> {point.y:.2f}'
        }
      }
    },
    series: [
      {
        name: 'LABEL.GRAPH.SERIES_NAME',
        colorByPoint: true,
        data: []
      }
    ]
  } as Highcharts.Options;

  constructor(
    private labelService: HttpLabelService,
    private modalService: NgbModal,
    private searchService: SearchService,
    private translateService: TranslateService
  ) {
    this.translateService.get('LABEL.LIST.GRAPH.TITLE').subscribe(title => {
      this.chartOptions.title.text = title;
    });
    this.translateService.get('LABEL.LIST.GRAPH.SERIES_NAME').subscribe(title => {
      this.chartOptions.series[0].name = title;
    });
  }

  ngOnInit() {
    this.searchSubscription = this.searchService.searchChangedObservable.subscribe(
      (search: SearchModel) => {
        this.defaultGroupId = search.groupId;
        this.defaultMonth = search.monthId;
        this.defaultYear = search.year;
        this.updateAllLabels();
      },
      () => {
        this.error = 'ERROR.GENERAL';
      }
    );
  }

  ngOnDestroy(): void {
    this.searchSubscription.unsubscribe();
  }

  onDelete(id: number) {
    this.isLoadingDelete = id;

    const modalRef = this.modalService.open(ModalComponent, { size: 'sm', centered: true });
    modalRef.result
      .then(x => {
        if (x === 'Delete') {
          // send delete command to the server
          this.labelService
            .deleteLabel(id)
            .then(() => {
              this.updateAllLabels();
            })
            .catch(() => {
              this.error = 'ERROR.GENERAL';
            });
        }
      })
      .catch(() => {
        this.error = 'ERROR.GENERAL';
      })
      .finally(() => {
        this.isLoadingDelete = null;
      });
    modalRef.componentInstance.title = 'LABEL.LIST.MODAL_DELETE.TITLE';
    modalRef.componentInstance.body = 'LABEL.LIST.MODAL_DELETE.BODY';
    modalRef.componentInstance.actionType = 'btn-danger';
    modalRef.componentInstance.actionName = 'DELETE';
  }

  private updateAllLabels() {
    if (
      (!this.defaultGroupId && this.defaultGroupId < 0) ||
      (!this.defaultMonth && this.defaultMonth < 0) ||
      (!this.defaultYear && this.defaultYear < 0)
    ) {
      this.error = 'ERROR.GENERAL';
      return;
    }

    this.isLoading = true;
    this.labelService
      .getLabels(this.defaultGroupId, this.defaultMonth, this.defaultYear)
      .then(labels => {
        this.loadItems(labels as LabelWithValuesModel[]);
      })
      .catch(() => {
        this.error = 'ERROR.GENERAL';
      })
      .finally(() => {
        this.isLoading = false;
      });
  }

  private loadItems(labels: LabelWithValuesModel[]) {
    this.labels = labels;

    const maxCurrentValueId = this.labels.reduce((prev, current) => {
      return prev.currentValue > current.currentValue ? prev : current;
    }).id;

    this.chartOptions.series[0].data = labels.map(x => {
      return {
        id: x.id,
        name: x.name,
        y: x.currentValue,
        sliced: x.id === maxCurrentValueId,
        selected: x.id === maxCurrentValueId
      };
    });
  }

  showForm() {
    return (
      !this.isLoading && !this.error && this.defaultGroupId >= 0 && this.defaultMonth >= 0 && this.defaultYear >= 0
    );
  }
}
