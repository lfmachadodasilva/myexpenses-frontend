import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import { HttpGroupService } from 'src/app/services/http/http-group.service';
import { GroupModel } from 'src/app/models/group.model';
import { MonthModel } from 'src/app/models/month.model';
import { SearchModel } from 'src/app/models/search.model';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  groups: GroupModel[] = [];
  groupId: number;

  years: number[] = [];
  year: number;

  months: MonthModel[] = [];
  monthId: number;

  isLoading = false;
  error: string = null;

  imgSearch = faSearch;

  // used by localStorage
  private groupKey = 'group';
  private monthKey = 'month';
  private yearKey = 'year';

  constructor(
    private groupService: HttpGroupService,
    private searchService: SearchService,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.loadMonth();
    this.loadYear();
    this.loadGroups();
  }

  private loadGroups() {
    this.isLoading = true;
    this.groupService
      .getGroups()
      .then(groups => {
        // groups from the server
        this.groups = groups;
        // selected group from localstorage
        const groupItem = localStorage.getItem(this.groupKey);

        if (this.groups.some(x => x.id === +groupItem)) {
          // valid group
          this.groupId = +groupItem;
        } else {
          // not valid group, select the fist one available
          this.groupId = this.groups.length > 0 ? this.groups[0].id : null;
        }
      })
      .catch(() => {
        this.error = 'ERROR.GENERAL';
      })
      .finally(() => {
        this.onSearch();
        this.isLoading = false;
      });
  }

  private loadMonth() {
    this.months = MonthModel.getAllMonths();
    const monthItem = localStorage.getItem(this.monthKey);
    if (monthItem && this.months.some(x => x.id.toString() === monthItem.toString())) {
      this.monthId = +monthItem;
    } else {
      this.monthId = new Date().getMonth();
      localStorage.setItem(this.monthKey, this.monthId.toString());
    }
  }

  private loadYear() {
    const nowYear = new Date().getFullYear();
    for (let i = 0; i < 10; i++) {
      this.years.push(nowYear - i);
    }

    const yearItem = localStorage.getItem(this.yearKey);
    if (yearItem && this.years.some(x => x.toString() === yearItem)) {
      this.year = +yearItem;
    } else {
      this.year = nowYear;
      localStorage.setItem(this.yearKey, this.year.toString());
    }
  }

  showForm() {
    return !this.isLoading && !this.error;
  }

  onChangeGroup(value: number) {
    if (this.groups.some(x => x.id.toString() === value.toString())) {
      // valid group
      this.groupId = value;
    } else {
      // not valid group, select the fist one available
      this.groupId = this.groups.length > 0 ? this.groups[0].id : null;
    }
    localStorage.setItem(this.groupKey, this.groupId.toString());
  }

  onChangeMonth(value: number) {
    this.monthId = value;
    localStorage.setItem(this.monthKey, this.monthId.toString());
  }

  onChangeYear(value: number) {
    this.year = value;
    localStorage.setItem(this.yearKey, this.year.toString());
  }

  onSearch() {
    this.searchService.changeSearch({
      groupId: this.groupId,
      monthId: this.monthId,
      year: this.year
    } as SearchModel);
  }
}
