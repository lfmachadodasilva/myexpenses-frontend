import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HighchartsChartModule } from 'highcharts-angular';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './pages/app.component';
import { ExpenseComponent } from './pages/expense/expense.component';
import { HeaderComponent } from './pages/header/header.component';
import { LabelAddEditComponent } from './pages/label/label-add-edit/label-add-edit.component';
import { LabelComponent } from './pages/label/label.component';
import { LabelListComponent } from './pages/label/label-list/label-list.component';
import { LoadingComponent } from './pages/shared/loading.component';
import { ModalComponent } from './pages/shared/modal/modal.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { SearchComponent } from './pages/search/search.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    ExpenseComponent,
    HeaderComponent,
    LabelAddEditComponent,
    LabelComponent,
    LabelListComponent,
    LoadingComponent,
    ModalComponent,
    PaymentComponent,
    SearchComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HighchartsChartModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    NgbActiveModal,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    {
      provide: LOCALE_ID,
      useValue: 'en-US'
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [ModalComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
