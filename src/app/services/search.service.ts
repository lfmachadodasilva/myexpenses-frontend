import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { SearchModel } from '../models/search.model';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  searchChangedObservable = new Subject<SearchModel>();

  changeSearch(search: SearchModel) {
    this.searchChangedObservable.next(search);
  }
}
