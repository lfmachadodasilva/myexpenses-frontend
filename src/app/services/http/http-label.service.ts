import { HttpParams } from '@angular/common/http';
import { HttpService } from './http.service';
import { Injectable } from '@angular/core';

import { LabelWithValuesModel, LabelModel } from '../../models/label.model';

@Injectable({
  providedIn: 'root'
})
export class HttpLabelService {
  private labelUrl = 'labels';

  constructor(private http: HttpService<LabelWithValuesModel | LabelModel>) {}

  /**
   * Get labels
   * @returns labels
   */
  getLabels(groupId: number, month: number, year: number): Promise<LabelWithValuesModel[] | LabelModel[]> {
    let params = new HttpParams();
    params = params.append('groupId', groupId.toString());
    params = params.append('month', month.toString());
    params = params.append('year', year.toString());

    return this.http.getArray(this.labelUrl, params).toPromise();
  }

  /**
   * Get label by id
   * @param id - id
   * @returns label
   */
  getLabel(id: number): Promise<LabelModel> {
    return this.http.get(this.labelUrl + `/${id}`).toPromise();
  }

  /**
   * Add label
   * @param label - new label
   */
  addLabel(label: LabelModel) {
    const labelWithValue = new LabelWithValuesModel(label.id, label.name);
    labelWithValue.groupId = label.groupId;

    return this.http.post(this.labelUrl, labelWithValue).toPromise();
  }

  /**
   * Update label
   * @param label - updated label
   */
  updateLabel(label: LabelModel) {
    return this.http.put(this.labelUrl, label).toPromise();
  }

  /**
   * Delete label
   * @param id - id
   */
  deleteLabel(id: number) {
    return this.http.delete(this.labelUrl, id).toPromise();
  }
}
