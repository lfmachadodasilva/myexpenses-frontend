import { HttpParams } from '@angular/common/http';
import { HttpService } from './http.service';
import { Injectable } from '@angular/core';

import { GroupModel } from '../../models/group.model';

@Injectable({
  providedIn: 'root'
})
export class HttpGroupService {
  private groupUrl = 'groups';

  constructor(private http: HttpService<GroupModel>) {}

  /**
   * Get groups
   * @returns groups
   */
  getGroups(): Promise<GroupModel[]> {
    const params = new HttpParams();
    return this.http.getArray(this.groupUrl, params).toPromise();
  }
}
