import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService<TModel> {
  private baseUrl = environment.apiUrl + '/api/';

  constructor(private http: HttpClient) {}

  get(url: string, params: any = null): Observable<TModel> {
    url = this.baseUrl + url;
    return this.http.get<TModel>(url, { params }).pipe(
      tap(response => {
        if (environment.consoleResponses) {
          console.log(url, response);
        }
      })
    );
  }

  getArray(url: string, params: any = null): Observable<TModel[]> {
    url = this.baseUrl + url;
    return this.http.get<TModel[]>(url, { params }).pipe(
      tap(response => {
        if (environment.consoleResponses) {
          console.log(url, response);
        }
      })
    );
  }

  post(url: string, model: TModel): Observable<TModel> {
    url = this.baseUrl + url;
    return this.http.post<TModel>(url, model).pipe(
      tap(response => {
        if (environment.consoleResponses) {
          console.log(url, response);
        }
      })
    );
  }

  put(url: string, model: TModel): Observable<TModel> {
    url = this.baseUrl + url;
    return this.http.put<TModel>(url, model).pipe(
      tap(response => {
        if (environment.consoleResponses) {
          console.log(url, response);
        }
      })
    );
  }

  delete(url: string, id: number) {
    url = this.baseUrl + url + `/${id}`;
    return this.http.delete<TModel>(url).pipe(
      tap(response => {
        if (environment.consoleResponses) {
          console.log(url, response);
        }
      })
    );
  }
}
