import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExpenseComponent } from './pages/expense/expense.component';
import { LabelAddEditComponent } from './pages/label/label-add-edit/label-add-edit.component';
import { LabelComponent } from './pages/label/label.component';
import { LabelListComponent } from './pages/label/label-list/label-list.component';
import { PaymentComponent } from './pages/payment/payment.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/label',
    pathMatch: 'full'
  },
  {
    path: 'expense',
    component: ExpenseComponent
  },
  {
    path: 'label',
    component: LabelComponent,
    children: [
      {
        path: '',
        component: LabelListComponent
      },
      {
        path: 'add',
        component: LabelAddEditComponent
      },
      {
        path: ':id/edit',
        component: LabelAddEditComponent
      }
    ]
  },
  {
    path: 'payment',
    component: PaymentComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
