import * as moment from 'moment';

export class MonthModel {
  id: number;
  name: string;

  private moment = moment();

  constructor(id: number) {
    this.id = id;
    this.name = this.moment.month(id).format('MMMM');
  }

  static getAllMonths(): MonthModel[] {
    const months: MonthModel[] = [];
    for (let i = 0; i < 12; i++) {
      months.push(new MonthModel(i));
    }
    return months;
  }
}
