export class LabelModel {
  id: number;
  name: string;

  groupId: number;

  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
}

export class LabelWithValuesModel extends LabelModel {
  currentValue: number;
  lastValue: number;
  averageValue: number;

  constructor(id: number, name: string) {
    super(id, name);

    this.currentValue = 0.0;
    this.lastValue = 0.0;
    this.averageValue = 0.0;
  }
}
