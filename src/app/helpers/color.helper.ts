import { Injectable } from '@angular/core';
// const color = require('color');
// import { Color } from 'color';

@Injectable({
  providedIn: 'root'
})
export class ColorHelper {
  private ratio = 0.618033988749895;
  private hue = Math.random();

  get(saturation = null, value = null) {
    this.hue += this.ratio;
    this.hue %= 1;

    if (typeof saturation !== 'number') {
      saturation = 0.5;
    }

    if (typeof value !== 'number') {
      value = 0.95;
    }

    // return color({
    //   h: this.hue * 360,
    //   s: saturation * 100,
    //   v: value * 100
    // });
  }
}
